<!doctype html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/ico/favicon.png">

    <title>Rio Full Turista beta - Os melhores lugares perto de você</title>
    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Parisienne' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Poiret+One' rel='stylesheet' type='text/css'>
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/flat-ui.css" rel="stylesheet">
    <link href="assets/css/demo.css" rel="stylesheet">
    <link href="assets/css/docs.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="assets/css/main.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js"></script>
      <script src="assets/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=237047559771714";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>
	<div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand logo-font" href=".">Rio Full Turista</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href=".">Home</a></li>
            <li><a href="#modal-oque-e" data-toggle="modal">O que é?</a></li>
            <!-- <li><a href="#contact">Entre com contato</a></li>-->
          </ul>
        </div><!--/.navbar-collapse -->
      </div>
    </div>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <h1 class="logo-font">Rio Full Turista <small class="label label-success font-min">beta</small></h1>
        <p>Rio Full Turista é um aplicativo web que fornece informações de locais e estabelicimentos da cidade do Rio de Janeiro de acordo com sua localidade.</p>
        <div class="col-lg-12 box-busca">
        <form method="post" class="form-inline" action="" id="">   
          <fieldset>
              <legend>Encontre aqui informações sobre os locais e estabelicimentos mais próximos:</legend>   
              <div class="form-group col-lg-6">
                  <label for="txtEndereco" class="sr-only">Onde você está:</label>
                  <input type="text" id="txtEndereco" class="form-control " name="txtEndereco" placeholder="Onde você está?"/>
              </div>

              <div class="form-group col-lg-4">
                <label for="txtServico" class="sr-only"></label>
                <select class="select-block btn-sm" id="txtServico">
                  <option value="">Escolha o serviço</option>
                  <option value="hospital">Unidade de Saúde</option>
                  <option value="police">Delegacias Policiais</option>
                  <option value="fire_station">Bombeiros</option>
                  <option value="embassy">Embaixada</option>
                  <option value="restaurant">Restaurantes</option>
                  <option value="food">Lanchonetes</option>
                  <option value="cafe">Café</option>
                  <option value="convenience_store">Loja de conveniência</option>
                  <option value="bank">Banco</option>
                  <option value="airport">Aeroporto</option>
                  <option value="establishment">Hotéis</option>
                  <option value="laundry">Lavanderia</option>
                  <option value="stadium">Estádios de Futebol</option>
                  <option value="car_rental">Locadora de Veículos</option>
                  <option value="taxi_stand">Ponto de taxi</option>
                  <option value="subway_station">Estação de Metrô</option>
                  <option value="bus_station">Ponto de Onibus</option>
                  <option value="train_station">Estação de Metrô</option>
                  <option value="church">Igreja</option>
                  <option value="pharmacy">Farmácia</option>
                  <option value="shopping_mall">Shopping Center</option>
                  <option value="beauty_salon">Salão de Beleza</option>
                  <option value="book_store">Livraria</option>
                  <option value="night_club">Boates</option>
                  <option value="museum">Museu</option>
                  <option value="art_gallery">Galeria de Artes</option>
                  <option value="zoo">Zoolígico</option>
                  <option value="veterinary_care">Consultório Veterinário</option>
                  <option value="gym">Academias</option>
                  <option value="hair_care">Cabelereiro</option>
                  <option value="car_wash">Lava jato</option>
                  <option value="car_repair">Oficina Mecânica</option>
                  
                </select>
              </div>  

              <div>
                  <input type="button" id="btnEndereco" class="btn btn-warning" name="btnEndereco" value="Localizar" />
              </div>
       
              <div >
              </div>
       
              <input type="hidden" id="txtLatitude" name="txtLatitude" />
              <input type="hidden" id="txtLongitude" name="txtLongitude" />
              <input type="hidden" id="txtDistancia" name="txtDistancia" />
              <input type="hidden" id="txtDuraco" name="txtDuracao" />
          </fieldset>
      </form>
      </div>
      </div>
    </div>


    <div id="mapa">     

    </div>

      <hr>
    </div> <!-- /container -->
    <div class="container">
      <div class="fb-like" data-href="http://riofullturista.tk" data-height="The pixel height of the plugin" data-colorscheme="light" data-layout="standard" data-action="like" data-show-faces="true" data-send="false"></div>
      <div id="adsense">
        <style>
          .riofullturista { width: 320px; height: 50px; }
          @media(min-width: 500px) { .riofullturista { width: 468px; height: 60px; } }
          @media(min-width: 800px) { .riofullturista { width: 768px; height: 90px; margin: 0 auto; } }
        </style>
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <!-- riofullturista -->
        <ins class="adsbygoogle riofullturista"
             style="display:inline-block"
             data-ad-client="ca-pub-2207739562838337"
             data-ad-slot="4915401602"></ins>
        <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
      </div>
      <footer>
        <p>&copy; Rio App 2013</p>
      </footer>
    </div>

    <div class="modal fade" id="modal-oque-e">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">O que é?</h4>
          </div>
          <div class="modal-body">
            <p>O Rio Full Turista <small class="label label-success">beta</small> é um aplicativo web de busca de locais e estabelecimentos baseado na sua localidade. É bem simples, você informa onde está, escolhe o serviço, e recebe várias sugestões próximas de você no mapa.</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <script>
      // Include the UserVoice JavaScript SDK (only needed once on a page)
      UserVoice=window.UserVoice||[];(function(){var uv=document.createElement('script');uv.type='text/javascript';uv.async=true;uv.src='//widget.uservoice.com/ASQX7CDiZoPBcQI9s4k0Q.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(uv,s)})();

      //
      // UserVoice Javascript SDK developer documentation:
      // https://www.uservoice.com/o/javascript-sdk
      //

      // Set colors
      UserVoice.push(['set', {
        accent_color: '#448dd6',
        trigger_color: 'white',
        trigger_background_color: 'rgba(46, 49, 51, 0.6)'
      }]);

      // Identify the user and pass traits
      // To enable, replace sample data with actual user traits and uncomment the line
      UserVoice.push(['identify', {
        //email:      'john.doe@example.com', // User’s email address
        //name:       'John Doe', // User’s real name
        //created_at: 1364406966, // Unix timestamp for the date the user signed up
        //id:         123, // Optional: Unique id of the user (if set, this should not change)
        //type:       'Owner', // Optional: segment your users by type
        //account: {
        //  id:           123, // Optional: associate multiple users with a single account
        //  name:         'Acme, Co.', // Account name
        //  created_at:   1364406966, // Unix timestamp for the date the account was created
        //  monthly_rate: 9.99, // Decimal; monthly rate of the account
        //  ltv:          1495.00, // Decimal; lifetime value of the account
        //  plan:         'Enhanced' // Plan name for the account
        //}
      }]);

      // Add default trigger to the bottom-right corner of the window:
      UserVoice.push(['addTrigger', { mode: 'contact', trigger_position: 'bottom-right' }]);

      // Or, use your own custom trigger:
      //UserVoice.push(['addTrigger', '#id', { mode: 'contact' }]);

      // Autoprompt for Satisfaction and SmartVote (only displayed under certain conditions)
      UserVoice.push(['autoprompt', {}]);
      </script>  

  <!-- Bootstrap core JavaScript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <!-- <script src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyAI4sHbzxT7LR4DSPR5JfvnjFmDi72LUF4&sensor=false"></script> -->
  <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&sensor=true"></script>
  <script src="assets/js/jquery.js"></script>
	<script src="assets/js/jquery.cookie.js"></script>
	<script src="assets/js/welcomerioapp.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
  <script src="assets/js/jquery.ui.touch-punch.min.js"></script>
  <script src="assets/js/bootstrap-select.js"></script>
	<script src="assets/js/jquery-ui.custom.min.js"></script>
  <script src="assets/js/mapa.js"></script>
  <script src="assets/js/place.js"></script>
	<script src="assets/js/main.js"></script
</body>
</html>