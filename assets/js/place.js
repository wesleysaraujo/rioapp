var map;
var infowindow;

function loadPlace(latitude, longitude, tipo) {
  var local = new google.maps.LatLng(latitude, longitude);

  map = new google.maps.Map(document.getElementById('mapa'), {
    scrollwheel: false,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    center: local,
    zoom: 12,
  });

  var request = {
    location: local,
    radius: 8000,
    types: [tipo]
  };
  infowindow = new google.maps.InfoWindow();
  var service = new google.maps.places.PlacesService(map);
  service.nearbySearch(request, callback);
  //
  //service.getDetails(request, callback);
}

function callback(results, status) {
  if (status == google.maps.places.PlacesServiceStatus.OK) {
    for (var i = 0; i < results.length; i++) {
      createMarker(results[i]);
      //console.log(results[i]);
    }
  }
}


function CalculaDistancia(local) {
  var urlDistancematrix = "http://maps.googleapis.com/maps/api/distancematrix/json?origins={0}&destinations={1}&mode=driving&language=pt-BR&sensor=false";
  //Adicionar endereço de origem
  urlDistancematrix = urlDistancematrix.replace("{0}", $("#txtEndereco").val());
  //Adicionar endereço de destino
  urlDistancematrix = urlDistancematrix.replace("{1}", local);
  //Pegar o retorno do distancematrix
  $.getJSON(urlDistancematrix,
    function (data) {
      if (data.status == "OK") {
            return data;
          /*
          $('#litResultado').html("<strong>Origem</strong>: " + data.origin_addresses +
          "<br /><strong>Destino:</strong> " + data.destination_addresses +
          "<br /><strong>Distância</strong>: " + data.rows[0].elements[0].distance.text +
          " <br /><strong>Duração</strong>: " + data.rows[0].elements[0].duration.text
          ); */
      }else{
          return 'erro';
      }
  });
}


function createMarker(place) {
  //Marker da origem
  var placeOri = new google.maps.LatLng($('#txtLatitude').val(), $('#txtLongitude').val());
  var marker2 = new google.maps.Marker({
    scrollwheel: false,
    map: map,
    position: placeOri,
    icon: 'assets/img/pin-eu.png'
  });

  marker2.setPosition(placeOri);

  //Marker dos pontos
  var placeLoc = place.geometry.location;
  var marker = new google.maps.Marker({
    scrollwheel: false,
    map: map,
    position: placeLoc,
    icon:  'assets/img/pin-rio.png'
  });
  var request =  {
      reference: place.reference
  };
  google.maps.event.addListener(marker,'click',function(){
    var service = new google.maps.places.PlacesService(map);
        service.getDetails(request, function(place, status) {
          if (status == google.maps.places.PlacesServiceStatus.OK) { 
            var contentStr = '<h5>'+place.name+'</h5><p>'+place.formatted_address;
            if (!!place.formatted_phone_number) contentStr += '<br>'+place.formatted_phone_number;
            if (!!place.website) contentStr += '<br><a target="_blank" href="'+place.website+'">'+place.website+'</a>';
            //if (distancia != 'erro') contentStr += '<br> Distancia'+distancia.rows[0].elements[0].distance.text+'</a>';
            infowindow.setContent(contentStr);
            infowindow.open(map,marker);
          } else { 
            var contentStr = "<h5>Sem resultados, status="+status+"</h5>";
            infowindow.setContent(contentStr);
            infowindow.open(map,marker);
          }
        });
    });
}

/*
$(document).ready(function(){
  $("#btnEndereco").click(function() {
          var latitude = $('#txtLatitude').val(), longitude = $('#txtLongitude').val();
          var tipo     = $('txtEndereco').val();

          if($('#txtEndereco').val() != "")
              loadPlace(latitude, longitude, tipo);
      })
});
*/