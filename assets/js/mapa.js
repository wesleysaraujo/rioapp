var geocoder;
var map;
var marker;
var service;
var resultList;
var infowindow;
 
function initialize() {
    var latlng = new google.maps.LatLng(-22.9035393, -43.20958689999998);
    var options = {
        scrollwheel: false,
        zoom: 13,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
 
    map = new google.maps.Map(document.getElementById("mapa"), options);
 
    geocoder = new google.maps.Geocoder();
 
    marker = new google.maps.Marker({
        scrollwheel: false,
        map: map,
        draggable: true,
        icon: 'assets/img/pin-rio.png'
    });
 
    marker.setPosition(latlng);
}
 
$(document).ready(function () {
    initialize();

    //Reverse
    google.maps.event.addListener(marker, 'drag', function () {
        geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) { 
                    $('#txtEndereco').val(results[0].formatted_address);
                    $('#txtLatitude').val(marker.getPosition().lat());
                    $('#txtLongitude').val(marker.getPosition().lng());
                }
            }
        });
    });

    function carregaLocal(latitude, longitude, tipo){
        var localidade = new google.maps.LatLng(latitude, longitude);

        map = new google.maps.Map(document.getElementById('mapa'), {
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
              center: localidade,
              zoom: 15
            });

          var request = {
            location: localidade,
            radius: '5000',
            types: [tipo]
          };

          service = new google.maps.places.PlacesService(map);
          service.search(request, callback);
          /*
          service.nearbySearch(request, function(status, results, pagination) {
            if (status != google.maps.places.PlacesServiceStatus.OK) {
             return;
            }
            resultList.addPlaces(results);
            if (pagination.hasNextPage) {
              resultList.button.onClick = pagination.nextPage;
            }
          });
          */
    }

    function callback(results, status) {
      if (status == google.maps.places.PlacesServiceStatus.OK) {
        for (var i = 0; i < results.length; i++) {
          var place = results[i];
          createMarker(results[i]);
        }
      }
    }  

    function carregarNoMapa(endereco) {
        geocoder.geocode({ 'address': endereco + ', Rio de Janeiro - Rio de Janeiro, Brasil', 'region': 'BR' }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    var latitude = results[0].geometry.location.lat();
                    var longitude = results[0].geometry.location.lng();
 
                    $('#txtEndereco').val(results[0].formatted_address);
                    $('#txtLatitude').val(latitude);
                    $('#txtLongitude').val(longitude);
 
                    var location = new google.maps.LatLng(latitude, longitude);
                    marker.setPosition(location);
                    marker.setIcon('assets/img/pin-eu.png');
                    map.setCenter(location);
                    map.setZoom(16);
                }
            }
        });
    }
    
    google.maps.event.addDomListener(window, 'load', initialize);

    $("#txtEndereco").blur(function() {
        if($(this).val() != "")
            carregarNoMapa($(this).val());
    });
});