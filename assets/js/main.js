var objeto = {
	init : 	function(){

		$("select").selectpicker({style: 'btn btn-primary', menuStyle: 'dropdown-inverse'})

		$("#btn-select").on('click', function(e){
			e.preventDefault();
			alert();
			$('#selectServico').val(this.rel).attr('selected', true);
			$('#txtEndereco').focus();
			return false;
		});

		$("#btnEndereco").click(function() {
	        var latitude = $('#txtLatitude').val(), longitude = $('#txtLongitude').val();
	        var tipo     = $('#txtServico').val();

	        //console.log('lat: '+latitude+' long:'+longitude+' tipo: '+tipo);

	        if($('#txtEndereco').val() != "")
	            loadPlace(latitude, longitude, tipo);
	    });

	    $("#txtEndereco").autocomplete({
	        source: function (request, response) {
	            geocoder.geocode({ 'address': request.term + 'Rio de Janeiro, Brasil', 'region': 'BR' }, function (results, status) {
	                response($.map(results, function (item) {
	                    return {
	                        label: item.formatted_address,
	                        value: item.formatted_address,
	                        latitude: item.geometry.location.lat(),
	                        longitude: item.geometry.location.lng()
	                    }
	                }));
	            })
	        },
	        select: function (event, ui) {
	            $("#txtLatitude").val(ui.item.latitude);
	            $("#txtLongitude").val(ui.item.longitude);
	            var location = new google.maps.LatLng(ui.item.latitude, ui.item.longitude);
	            marker.setPosition(location);
	            map.setCenter(location);
	            map.setZoom(16);
	        }
	    });

	    if($(document).width() < 768){
	    	$('#adsense').remove();
	    }
		/*$('#btn-pesquisa').on('click', function(e){
				e.preventDefault();

				$('#resultado .item').remove();

				var app, list_results;
				var bairro = $('#bairro').val();
				app  = new welcomeRioApp('7fb208d4e3b63cf34d709d71d90a77da','eebo8-iwtu0-qob5a'); // Intancia o objeto com as credenciais
				app.auth() ; // Tenta se autenticar no riodatamine.com.br 

				list_results = app.getAllReferences(bairro).results; // Obtem em formato JSON as localizações

				for( i = 0; list_results.length > i; i++ ){

					// DESCRIÇÃO list_results[i].description.text
					// NOME list_results[i]
					// SITE list_results[i].contactData.website / .email
					// INFORMAÇÕES DE HORARIO  list_results[i].characteristics.general_info
					// GEOLOCATION list_results[i].geoResult /.address/.neighbourhood/ .point.lat / .point.lng
					// CATEGORIA list_results[i].taxonomies[0].type
					// IMAGEM list_results[i].files[0].file
					$('#resultado').append('<div class="item"><h1>'+unescape(list_results[i].name)+'</h1>'+'<small>'+unescape(list_results[i].description.text)+'</small><p> Endereço: '+list_results[i].geoResult.address+' Bairro: '+bairro+'</p>'+'</div>');
					//$('#resultado').append('<div class="foto"><img src="'+list_results[i].files[0].file+'" /></div>');
					console.log(list_results[i]);
				}	



				return false;
		});
		*/
	}
}

$(function(){
	objeto.init();
});